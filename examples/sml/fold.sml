datatype 'a tree = null
		 | node of 'a * 'a tree * 'a tree

					     (* null : 'a tree *)
					     (* node : 'a * 'a tree * 'a tree -> 'a tree *)

(*

fold :   'summary
     ->  'a * 'summary * 'summary -> 'summary
     -> 'a tree
     -> 'summary

*)

datatype 'a List = Nil
		 | Cons of 'a * 'a List

				   (* Nil : 'a List *)
				   (* Cons : 'a * 'a List -> 'a List *)

				   (* fold : 'summary   (* Nil *)
                                           -> 'a * 'summary -> summary  (* Cons *)
                                           -> 'a List
                                           -> 'summary *)

			   (*

fun foldList (nilC : 'summary) (conC : 'a * 'summary -> 'summary) (l : 'a List) =    *)


fun foldList nilC  _    Nil   : 's          = nilC
  | foldList nilC conC (Cons (x , xs)) = conC (x , foldList nilC conC xs)
