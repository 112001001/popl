# Starting with Rust

- Deadline :: Nov 3, 2023. 18:00.

1. Please follow the rust book and implement a hello world program
   both as a standalone program (compiled using rustc) and as a
   package using (cargo)

   <https://doc.rust-lang.org/book/ch01-00-getting-started.html>

2. Download the [`feature.rs`][features.rs] file from the repository
   and try out various examples of move semantics and ownership.

3. Implement a type with copy semantics instead of move semantics by defining
   the `Clone` and `Copy` trait

4. Define a simpler resource type with an explicit `Drop` which just
   prints a message (so that you can know when the drop happens) trait
   for the type above. Try out the code with sample code to get an
   idea of how rust drops stuff.

[features.rs]: <../examples/rust/features.rs>
